use grid::Grid;
use std::{thread, time};
use terminal_size::{terminal_size, Height, Width};

fn check(row: isize, col: isize, population: &Grid<bool>) -> u8 {
    let mut neighbors = 0;
    for r in (&row - 1)..=(&row + 1) {
        for c in (&col - 1)..=(&col + 1) {
            if c as usize <= population.cols() - 1
                && c >= 0
                && r as usize <= population.rows() - 1
                && r >= 0
                && population.get(r as usize, c as usize) == Some(&true)
            {
                neighbors += 1;
            }
        }
    }
    neighbors
}

fn tick(population: &mut Grid<bool>) {
    let mut changes: Vec<(usize, usize, bool)> = Vec::new();
    for r in 0..population.rows() {
        for c in 0..population.cols() {
            let neighbors = check(r as isize, c as isize, population);

            match population.get(r, c) {
                Some(&true) => match neighbors {
                    0 | 1 => {
                        // Pop dies from underpopulation
                        changes.push((r, c, false));
                    }
                    2 | 3 => {
                        // Pop survies
                        changes.push((r, c, true));
                    }
                    _ => {
                        // Pop dies form overpopulation
                        changes.push((r, c, false));
                    }
                },
                Some(&false) => match neighbors {
                    3 => {
                        // Pop comes to life
                        changes.push((r, c, true));
                    }
                    _ => {
                        // Pop stays dead
                        changes.push((r, c, false));
                    }
                },
                None => {
                    panic!("Tried to access location out of grid");
                }
            }
        }
    }
    for i in changes {
        population.replace(i.0, i.1, i.2);
    }
}

fn show_population(population: &Grid<bool>) {
    println!("==========");
    for i in 0..population.rows() {
        for j in population.iter_row(i) {
            if *j {
                print!("*");
            } else {
                print!(" ");
            }
        }
        println!();
    }
}

#[allow(dead_code)]
fn create_map(population: &Grid<bool>) -> String {
    let mut field = String::from("");
    for i in 0..population.rows() {
        for j in population.iter_row(i) {
            if *j {
                field.push('*');
            } else {
                field.push(' ');
            }
        }
        field.push('\n');
    }
    field
}

fn populate(land: &mut Grid<bool>) {
    for i in land.iter_mut() {
        *i = rand::random::<bool>();
    }
}

fn create_land(row: usize, col: usize) -> Grid<bool> {
    Grid::new(row, col)
}

fn run(row: usize, col: usize) {
    let mut land = create_land(row, col);
    populate(&mut land);
    loop {
        show_population(&land);
        tick(&mut land);
        thread::sleep(time::Duration::from_secs(1));
    }
}

fn main() {
    let window = terminal_size();

    if let Some((Width(w), Height(h))) = window {
        run(h as usize, w as usize);
    } else {
        println!("Unable to get terminal size");
    }
}

#[test]
fn create_land_small_test() {
    use grid::grid;
    assert_eq!(create_land(2, 2), grid![[false, false] [false, false]]);
}

#[test]
fn create_land_large_test() {
    use grid::grid;
    let grid = grid![[false, false, false, false, false, false, false, false, false, false]
                               [false, false, false, false, false, false, false, false, false, false]
                               [false, false, false, false, false, false, false, false, false, false]
                               [false, false, false, false, false, false, false, false, false, false]
                               [false, false, false, false, false, false, false, false, false, false]
                               [false, false, false, false, false, false, false, false, false, false]
                               [false, false, false, false, false, false, false, false, false, false]
                               [false, false, false, false, false, false, false, false, false, false]
                               [false, false, false, false, false, false, false, false, false, false]
                               [false, false, false, false, false, false, false, false, false, false]];
    assert_eq!(create_land(10, 10), grid);
}

#[test]
fn show_population_test() {
    use grid::grid;
    let grid = grid![[false, true,  false, false, true,  false, false, false, false, false]
                               [true,  false, true,  false, false, false, false, false, false, false]
                               [false, true,  false, false, false, false, false, false, false, false]
                               [false, false, false, false, false, false, false, false, false, false]
                               [false, false, false, false, false, false, false, false, false, false]
                               [false, false, false, false, false, false, false, false, false, false]
                               [false, false, false, false, false, false, false, false, false, false]
                               [false, false, false, false, false, false, false, false, false, false]
                               [false, false, false, false, false, false, false, false, false, false]
                               [false, false, false, false, false, false, false, false, false, false]];
    let field = ".*..*.....\n*.*.......\n.*........\n..........\n..........\n..........\n..........\n..........\n..........\n..........\n";
    assert_eq!(create_map(&grid), field);
}

#[test]
fn check_test() {
    use grid::grid;
    let grid = grid![ [false, true,  false, false, true]
                                    [true,  false, true,  false, false]
                                    [false, true,  false, false, false]
                                    [false, false, false, false, false]
                                    [false, false, false, false, false]];
    assert_eq!(check(1, 1, &grid), 4);
}

#[test]
fn tick_1_test() {
    use grid::grid;
    let mut land = grid![ [false, true,  false, false, true]
                                    [true,  false, true,  false, false]
                                    [false, true,  false, false, false]
                                    [false, false, false, false, false]
                                    [false, false, false, false, false]];
    tick(&mut land);
    let field = ".*...\n*.*..\n.*...\n.....\n.....\n";
    assert_eq!(create_map(&land), field);
}
#[test]
fn tick_2_test() {
    use grid::grid;
    let mut land = grid![ [false, true,  false, false, true]
                                    [true,  false, true,  false, false]
                                    [false, true,  false, true,  false]
                                    [false, false, false, false, false]
                                    [false, false, false, false, false]];
    tick(&mut land);
    let field = ".*...\n*..*.\n.***.\n.....\n.....\n";
    assert_eq!(create_map(&land), field);
}
